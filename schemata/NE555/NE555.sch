EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 4000 4000
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 61721FD2
P 2050 2300
F 0 "#PWR?" H 2050 2050 50  0001 C CNN
F 1 "GND" H 2055 2127 50  0000 C CNN
F 2 "" H 2050 2300 50  0001 C CNN
F 3 "" H 2050 2300 50  0001 C CNN
	1    2050 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 617233C8
P 1250 2300
F 0 "#PWR?" H 1250 2050 50  0001 C CNN
F 1 "GND" H 1255 2127 50  0000 C CNN
F 2 "" H 1250 2300 50  0001 C CNN
F 3 "" H 1250 2300 50  0001 C CNN
	1    1250 2300
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5G BTN
U 1 1 61723A0F
P 1250 1800
F 0 "BTN" V 1250 1752 50  0000 R CNN
F 1 "SW_MEC_5G" V 1205 1752 50  0001 R CNN
F 2 "" H 1250 2000 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=488" H 1250 2000 50  0001 C CNN
	1    1250 1800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R 1kΩ
U 1 1 61724B51
P 1250 1200
F 0 "1kΩ" H 1320 1200 50  0000 L CNN
F 1 "R" H 1320 1155 50  0001 L CNN
F 2 "" V 1180 1200 50  0001 C CNN
F 3 "~" H 1250 1200 50  0001 C CNN
	1    1250 1200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 6172515C
P 1250 1000
F 0 "#PWR?" H 1250 850 50  0001 C CNN
F 1 "VCC" H 1265 1173 50  0000 C CNN
F 2 "" H 1250 1000 50  0001 C CNN
F 3 "" H 1250 1000 50  0001 C CNN
	1    1250 1000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 617254CB
P 2050 1000
F 0 "#PWR?" H 2050 850 50  0001 C CNN
F 1 "VCC" H 2065 1173 50  0000 C CNN
F 2 "" H 2050 1000 50  0001 C CNN
F 3 "" H 2050 1000 50  0001 C CNN
	1    2050 1000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC OUT
U 1 1 61725D61
P 2750 1400
F 0 "OUT" V 2765 1528 50  0000 L CNN
F 1 "VCC" V 2765 1528 50  0001 L CNN
F 2 "" H 2750 1400 50  0001 C CNN
F 3 "" H 2750 1400 50  0001 C CNN
	1    2750 1400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61730815
P 2600 2300
F 0 "#PWR?" H 2600 2050 50  0001 C CNN
F 1 "GND" H 2605 2127 50  0000 C CNN
F 2 "" H 2600 2300 50  0001 C CNN
F 3 "" H 2600 2300 50  0001 C CNN
	1    2600 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C?
U 1 1 6173181D
P 2600 2050
F 0 "C?" H 2718 2096 50  0000 L CNN
F 1 "CP" H 2718 2005 50  0000 L CNN
F 2 "" H 2638 1900 50  0001 C CNN
F 3 "~" H 2600 2050 50  0001 C CNN
	1    2600 2050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 61739E66
P 1500 1000
F 0 "#PWR?" H 1500 850 50  0001 C CNN
F 1 "VCC" H 1515 1173 50  0000 C CNN
F 2 "" H 1500 1000 50  0001 C CNN
F 3 "" H 1500 1000 50  0001 C CNN
	1    1500 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1800 1500 1800
Wire Wire Line
	1500 1800 1500 1000
Wire Wire Line
	1250 1000 1250 1050
Wire Wire Line
	1250 2000 1250 2300
Wire Wire Line
	2600 2200 2600 2300
Wire Wire Line
	2050 1000 2050 1200
Wire Wire Line
	2550 1600 2600 1600
Wire Wire Line
	2550 1800 2600 1800
Wire Wire Line
	2600 1800 2600 1900
Connection ~ 2600 1800
Wire Wire Line
	2750 1400 2550 1400
Connection ~ 2600 1600
Wire Wire Line
	2600 1600 2600 1800
Wire Wire Line
	2050 2000 2050 2300
Wire Wire Line
	2600 1350 2600 1600
Wire Wire Line
	2600 1000 2600 1050
$Comp
L power:VCC #PWR?
U 1 1 6173A1FB
P 2600 1000
F 0 "#PWR?" H 2600 850 50  0001 C CNN
F 1 "VCC" H 2615 1173 50  0000 C CNN
F 2 "" H 2600 1000 50  0001 C CNN
F 3 "" H 2600 1000 50  0001 C CNN
	1    2600 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:R 1kΩ?
U 1 1 61730009
P 2600 1200
F 0 "1kΩ?" H 2670 1200 50  0000 L CNN
F 1 "R" H 2670 1155 50  0001 L CNN
F 2 "" V 2530 1200 50  0001 C CNN
F 3 "~" H 2600 1200 50  0001 C CNN
	1    2600 1200
	1    0    0    -1  
$EndComp
$Comp
L ne555p:NE555P NE555P
U 1 1 61726FF8
P 2050 1600
F 0 "NE555P" H 2050 2181 50  0001 C CNN
F 1 "NE555P" H 2050 2089 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 2700 1200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ne555.pdf" H 2900 1200 50  0001 C CNN
	1    2050 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1350 1250 1400
Wire Wire Line
	1550 1400 1250 1400
Connection ~ 1250 1400
Wire Wire Line
	1250 1400 1250 1600
$EndSCHEMATC
