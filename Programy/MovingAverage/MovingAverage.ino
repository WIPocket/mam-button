#define PIN 2
#define DEPTH 40

bool history[DEPTH];
bool pressed = false;
int i = 0, sum = 0, counter = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  sum -= history[i];
  history[i] = digitalRead(PIN);
  sum += history[i];

  i = (i + 1) % DEPTH;

  if (sum > DEPTH/2 && !pressed){
    pressed = true;
    counter++;
    Serial.println("Stisknuto po " + String(counter));
  }
  if (sum < DEPTH/2) pressed = false;
}
