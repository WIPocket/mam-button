#define PIN 2
#define TIMEOUT 10

volatile int counter = 0;
volatile unsigned long lastPressedTime;
volatile bool pressedLast = false;

void setup() {
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(PIN), handleChange, CHANGE);
}

void handleChange() {
  bool pressed = digitalRead(PIN);
  unsigned long currentTime = millis();

  if(pressed && !pressedLast){
    if (currentTime - lastPressedTime >= TIMEOUT){
      counter++;
      Serial.println("Stisknuto po " + String(counter));
    }
    lastPressedTime = currentTime;
  }

  pressedLast = pressed;
}




void loop(){}
