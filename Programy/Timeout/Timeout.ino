#define PIN 2
#define TIMEOUT 10

int counter = 0;
unsigned long lastPressedTime;
bool pressedLast = false;

void setup() {
  Serial.begin(9600);
}

void loop() {
  bool pressed = digitalRead(PIN);
  if(pressed && !pressedLast){
    if (millis() - lastPressedTime >= TIMEOUT){
      counter++;
      Serial.println("Stisknuto po " + String(counter));
    }
    lastPressedTime = millis();
  }
  pressedLast = pressed;
}
